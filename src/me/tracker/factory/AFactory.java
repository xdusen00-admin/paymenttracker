package me.tracker.factory;

import me.tracker.statement.Statement;

public abstract class AFactory {

	/**
	 * evalue cmd to Statement
	 * @param cmd
	 * @return {@link Statement}
	 */
	public abstract Statement satisfies(String cmd);
	
}
