package me.tracker.factory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.tracker.model.BankState;
import me.tracker.statement.CurrencyRate;
import me.tracker.statement.CurrencyStatement;
import me.tracker.statement.ExtendPrint;
import me.tracker.statement.FileLoadStatement;
import me.tracker.statement.NOOPStatement;
import me.tracker.statement.QuitStatement;
import me.tracker.statement.Statement;


public class StatementFactory extends AFactory {

	enum OP{
		CURRENCY("(^[A-Z]{3})\\s+([+-]?\\d+)$"),
		CURRENCYRATE("^def([A-Z]{3})\\s+(\\d+[.]\\d+)$"),
		EXTENDRATEPRINT("^def(enable|disable)\\s+rate$"),
		LOADFILE("^(?i)load\\s+([a-z]?:?[.a-z\\/\\\\]+)$"),
		QUIT("^quit$"),
		NOOP("^\n$"); // or .* to match all
		
		private final String regExp;
		
		private OP(String re){
			regExp = re;
		}
		
		public String expression() {
			return regExp;
		}			
	}
	
	private Matcher matched;
	private final BankState bankState;
	
	public StatementFactory(BankState bankState) {
		this.bankState = bankState;
	}
	
	@Override
	public Statement satisfies(String cmd) {
		OP operation = match(cmd);
		switch (operation) {
		case CURRENCY:					
			return new CurrencyStatement()
					.withBankState(bankState)
					.withCurrency(matched.group(1))
					.withAmount(Integer.parseInt(matched.group(2)));
		case CURRENCYRATE:			
			return new CurrencyRate()
					.withBankState(bankState)
					.withCurrency(matched.group(1))
					.withRate(Double.parseDouble(matched.group(2)));
		case QUIT:
			return new QuitStatement()
					.withBankState(bankState);
		case LOADFILE:
			return new FileLoadStatement()
					.withState(bankState)
					.withFilePath(matched.group(1));
		case EXTENDRATEPRINT:			
			return new ExtendPrint()
					.withBankState(bankState)
					.withEnabled(matched.group(1).equals("enable"));
		case NOOP:
			return new NOOPStatement();
		}
		return null;
	}
	
	private OP match(String cmd) {		
		for(OP op : OP.values()){
			matched = Pattern.compile(op.expression()).matcher(cmd);
			if(matched.find()){
				return op;
			}
		}
		return OP.NOOP;
	}

}
