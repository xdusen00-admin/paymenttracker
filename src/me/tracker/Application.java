package me.tracker;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;
import me.tracker.factory.AFactory;
import me.tracker.factory.StatementFactory;
import me.tracker.model.BankState;


public class Application {

	private BufferedReader console;
	private AFactory command;	
	private final int DELAY = 60000;
	private final BankState bankState;
	
	public Application(BankState state) {
		this.bankState = state;
		command = new StatementFactory(bankState);		
	}
	
	public void repl() {		
		InputStreamReader isr = new InputStreamReader(System.in);
		console = new BufferedReader(isr);
		String cmd = "";
		try {
			while((cmd = console.readLine()) != null){
				command.satisfies(cmd).eval();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void initPrintEvent() {
		Timer ttimer = new Timer(true); 
		ttimer.scheduleAtFixedRate(new TimerTask() {
			
			@Override
			public void run() {
				System.out.print(bankState.pprint());
				
			}
		}, DELAY,DELAY); // first event after DELAY period
	}
	
	/**
	 * starts timerTask and run loop
	 */
	public void run() {
		initPrintEvent();
		repl();
	}

	public static void main(String[] args){
		BankState bankState = new BankState();		
		if(args.length == 1){
			if (new File(args[0]).exists()) 
				bankState.initFromFile(args[0]);
			else
				System.err.println("file not found at given path ");
		}
		 
		Application app = new Application(bankState);
		app.run();
	}
	
}
