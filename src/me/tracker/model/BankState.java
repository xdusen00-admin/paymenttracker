package me.tracker.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * BankState is model keep data for application
 * 
 * @author torsky
 *
 */
public final class BankState {
	
	private Map<String, Integer> data;
	private Map<String, Double> rate;
	private DecimalFormat df = new DecimalFormat("#0.00");
	private Matcher matched;
	private boolean rateEnabled;	
	
	public BankState() {
		data = new TreeMap<String,Integer>(Collections.reverseOrder());  
		rate = new HashMap<>(); 
		rateEnabled = false;
	}

	public void add(String currency, Integer amount) {
		if(data.containsKey(currency))		
			amount += data.get(currency);
		data.put(currency, amount);
	}
	
	public synchronized Integer amount(String currency) {
		return data.get(currency);
	}
	

	public void defExchangePrinting(boolean enabled) {
		this.rateEnabled = enabled;
	}
	
	public String pprint() {		
		StringBuilder sb = new StringBuilder();		
		
		for(Entry<String, Integer> entry : data.entrySet()){
			if(entry.getValue() != 0){
				sb.append(pprint(entry) + "\n");				
			}
		}
		return sb.toString();
	}
	
	private String pprint(Entry<String, Integer> entry) {		
		return entry.getKey()+" "+entry.getValue() + (rateEnabled ? rateEntryFormat(entry.getKey(),entry.getValue()) : "" );
	}

	private String rateEntryFormat(String currency, Integer amount) {
		if(currency.equals("USD")) return "";
		Double rateVal = 0d;
		if((rateVal = rate.get(currency)) != null){
			 return " (USD "+ df.format(rateVal * amount)+")";
		}
		return " (USD not def)";
	}

	public synchronized String pprint(String currency) {
		Integer amount = data.get(currency);
		return currency + " " + amount + (rateEnabled ? rateEntryFormat(currency, amount) : "");
	}

	public synchronized void initFromFile(String filePath) {
		try{
			BufferedReader br = new BufferedReader(
					new InputStreamReader(new FileInputStream(new File(filePath))));
			String line = "";
			while((line = br.readLine()) != null){
				FTYPE wasLoaded = matchType(line);				
					switch (wasLoaded) {					
					case AMOUNT: // currency;amount
						add(matched.group(1), Integer.parseInt(matched.group(2)));
						break;
					case RATE: // currency;rate
						add(matched.group(1), Double.parseDouble(matched.group(2)));
						break;
					case IDOLAST: // currency;amount;rate
						add(matched.group(1), Integer.parseInt(matched.group(2)));
						add(matched.group(1), Double.parseDouble(matched.group(3)));
						break;
					default:
						System.err.println("bad formated line: " + line);
						break;
					}								
			}
			br.close();
		}catch(IOException e){
			System.err.println("state can not be initialized");
			System.err.println(e.getMessage());
		}		
	}

	private FTYPE matchType(String line) {			
		for(FTYPE fft : FTYPE.values()){
			matched = Pattern.compile(fft.re()).matcher(line);
			if(matched.find()){
				return fft;
			}
		}
		return FTYPE.ERR;
	}

	public void add(String currency, Double currencyRate) {
		if(currencyRate > 0d)
			rate.put(currency, currencyRate);
	}

	public void storeToFile(String fname) {
		try {
			File f = new File(fname);
			String prepend = "";
			BufferedWriter bw = new BufferedWriter(new FileWriter(f));
			for(Entry<String, Integer> e : data.entrySet()){
				bw.write(prepend+e.getKey()+";"+e.getValue()+";"+ (rate.containsKey(e.getKey()) ? df.format(rate.get(e.getKey())) : "0.0"));
				rate.remove(e.getKey());
				prepend = "\n";				
			}
			for(Entry<String, Double> e : rate.entrySet()){
				bw.write(prepend+e.getKey()+";"+df.format( e.getValue() ));
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	enum FTYPE{
		AMOUNT("^([A-Z]{3});([+-]?\\d+)$"),
		RATE("^([A-Z]{3});(\\d+.\\d+)$"),
		IDOLAST("^([A-Z]{3});([+-]?\\d+);(\\d+.\\d+)$"),
		ERR("\n");
		
		private String re;
		
		private FTYPE(String re){
			this.re = re;
		}
		
		public String re() {
			return re;
		}
	}
	
	public Double rate(String currency) {		
		return rate.get(currency);
	}

	/**
	 * for testing purpose
	 */
	public synchronized void addAll() {
		Random rnd = new Random();
		int xx = 0;
		for(char ch1 = 65; ch1< 91;ch1++){
			for(char ch2 = 65; ch2< 91;ch2++){
				for(char ch3 = 65; ch3< 91;ch3++){
					int a = rnd.nextInt(1000)+1;
					double r = rnd.nextDouble();
					String str = new String(ch1+""+ch2+""+ch3+"");
					xx++;
					if(str.equals("XYZ")){
						a = 1001;
					}
					if(xx < 17000){
						add(str, a);
					}else{
//						System.out.println(str);
					}
					add(str, r);
					if(str.equals("ZDV")){
						add(str, 0.5);
					}
					
					
				}
			}
		}
		storeToFile("ido.last");
	}	
}
