package me.tracker.statement;

import java.util.Map;

public interface Statement {

	public Map<String, String> eval();
	
}
