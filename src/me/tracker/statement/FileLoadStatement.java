package me.tracker.statement;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import me.tracker.model.BankState;

public class FileLoadStatement implements Statement {

	BankState state;
	String filePath;
	
	public FileLoadStatement() { }
	
	public FileLoadStatement withState(BankState state) {
		this.state = state;
		return this;
	}

	public FileLoadStatement withFilePath(String filePath) {
		this.filePath = filePath;
		return this;
	}

	@Override
	public Map<String, String> eval() {
		boolean fileExist = false;
		// is entered defaut ? load ido.last
		if(filePath.equals("default")){
			if((fileExist = new File("ido.last").exists())){
				state.initFromFile("ido.last");
			}			
		}else{ // load file from given path
			if((fileExist = new File(filePath).exists())){
				state.initFromFile(filePath);
			}
		}
		return fileExist ?
				new HashMap<String,String>(){
					private static final long serialVersionUID = 1L;
				{
					put("info","loaded file " + filePath);
				}} :
				new HashMap<String,String>(){
					private static final long serialVersionUID = 1L;
				{
					put("error","file " + filePath + " was not found");
				}};
	}

}
