package me.tracker.statement;

import java.util.HashMap;
import java.util.Map;

import me.tracker.model.BankState;

public class QuitStatement implements Statement {

	private BankState state;
	
	public QuitStatement() {
		
	}
	
	public QuitStatement withBankState(BankState bankState) {
		this.state = bankState;
		return this;
	}
	
	@Override
	public Map<String, String> eval() {
		state.storeToFile("ido.last");
		System.out.println("bye!");
		System.exit(0);
		return new HashMap<String,String>(){
			private static final long serialVersionUID = 1L;
		{
			put("info", "bye!");
		}};
	}

}
