package me.tracker.statement;

import java.util.HashMap;
import java.util.Map;

import me.tracker.model.BankState;

public class CurrencyRate implements Statement {

	private String currency;
	private Double rate;
	private BankState state;
	
	public CurrencyRate() { }
	
	public CurrencyRate withCurrency(String currency) {
		this.currency = currency;
		return this;
	}
	
	public CurrencyRate withRate(Double rate) {
		this.rate = rate;
		return this;
	}
	
	public CurrencyRate withBankState(BankState bankState) {
		this.state = bankState;
		return this;
	}

	@Override
	public Map<String, String> eval() {
		state.add(currency, rate);
		return new HashMap<String, String>(){
			private static final long serialVersionUID = 1L;
		{
			put("info", "currency rate for " + currency + " was set to " + rate);
		}};
	}	

}
