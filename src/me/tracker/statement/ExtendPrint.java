package me.tracker.statement;

import java.util.HashMap;
import java.util.Map;

import me.tracker.model.BankState;

public class ExtendPrint implements Statement{

	private boolean enabled;
	private BankState state;
	
	public ExtendPrint() { }

	public ExtendPrint withEnabled(boolean enabled) {
		this.enabled = enabled;
		return this;
	}
	
	public ExtendPrint withBankState(BankState bankState) {
		this.state = bankState;
		return this;
	}

	@Override
	public Map<String, String> eval() {
		state.defExchangePrinting(this.enabled);
		return new HashMap<>();
	}
	
	
}
