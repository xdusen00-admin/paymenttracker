package me.tracker.statement;

import java.util.HashMap;
import java.util.Map;

import me.tracker.model.BankState;

public class CurrencyStatement implements Statement{

	private String currency;
	private Integer amount;
	private BankState state;
	
	public CurrencyStatement() {
		this.currency = "";
		this.amount = 0;
	}
	

	public CurrencyStatement withCurrency(String currency) {
		this.currency = currency;
		return this;
	}

	public CurrencyStatement withAmount(Integer amount) {
		this.amount = amount;
		return this;
	}

	public CurrencyStatement withBankState(BankState bankState) {
		this.state = bankState;
		return this;
	}	
	
	@Override
	public Map<String, String> eval() {
		state.add(currency, amount);		
		return new HashMap<String, String>(){
			private static final long serialVersionUID = 1L;
		{
			put("info", state.pprint(currency));
		}};
	}	
}
