package me.tracker.statement;

import java.util.HashMap;
import java.util.Map;

public class NOOPStatement implements Statement {

	@Override
	public Map<String, String> eval() {		
		System.err.println("bad command");
		return new HashMap<String, String>(){
			private static final long serialVersionUID = 1L;
		{
			put("error", "bad command");
		}};
	}

}
