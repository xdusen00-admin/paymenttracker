package me.tracker.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestBankState {

	BankState state;
	
	@Before
	public void setUp() throws Exception {
		state = new BankState();
		state.initFromFile("bankState.csv");
	}

	@Test
	public void testBankState() {
		assertEquals(new Integer(2000), state.amount("RMB"));
		assertEquals(new Integer(100), state.amount("EMD"));
		assertEquals(new Double(0.4), state.rate("EMD"));
		state.defExchangePrinting(true);
		assertEquals("EMD 100 (USD 40.00)", state.pprint("EMD"));		
		assertEquals("RMB 2000 (USD 1000.00)", state.pprint("RMB"));
		state.add("RMB", -100);
		assertEquals(new Integer(1900), state.amount("RMB"));
		state.add("EMD", 800);
		assertEquals(new Integer(900), state.amount("EMD"));
	
	}
	
}
