package me.tracker.model;

import static org.junit.Assert.*;

import java.util.TimerTask;

import org.junit.Before;
import org.junit.Test;

public class TestBankStateConcurrentAccess {

	BankState state;
	
	@Before
	public void setUp() throws Exception {
		state = new BankState();		
	}

	@Test
	public void test() throws InterruptedException {
		BankState state = new BankState();
		java.util.Timer ttimer = new java.util.Timer(true); 
        
		ttimer.scheduleAtFixedRate(new TimerTask() {
			
			@Override
			public void run() {
				// amount is synchronized method
				assertNotNull(state.amount("USD"));				
			}
		}, 0, 1000);
        
        state.addAll(); // longest possible task, synchorized

	}

}
