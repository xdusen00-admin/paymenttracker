package me.tracker.statements;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import me.tracker.factory.AFactory;
import me.tracker.factory.StatementFactory;
import me.tracker.model.BankState;

public class TestCurrencyRate {

	BankState state;
	AFactory factory;
	
	@Before
	public void setUp() throws Exception {		
		state = new BankState();		
		state.initFromFile("bankState.csv");
		factory = new StatementFactory(state);
	}

	@Test
	public void testCurrencyExachaningAndExtendPrint() {
		factory.satisfies("defenable rate").eval();
		factory.satisfies("defRMB 0.5").eval();
		assertEquals("RMB 2000 (USD 1000.00)", state.pprint("RMB"));
		factory.satisfies("defHUG 0.1").eval();
		factory.satisfies("defdisable rate").eval();
		assertTrue("Currency and pos number", factory.satisfies("HUG 100").eval().get("info").equals("HUG 100"));
		factory.satisfies("defenable rate").eval();
		assertEquals("HUG 100 (USD 10.00)", state.pprint("HUG"));
		assertEquals("HUD 100 (USD not def)", factory.satisfies("HUD 100").eval().get("info"));
	}

}
