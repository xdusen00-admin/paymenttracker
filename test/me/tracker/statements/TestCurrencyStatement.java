package me.tracker.statements;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import me.tracker.factory.AFactory;
import me.tracker.factory.StatementFactory;
import me.tracker.model.BankState;

public class TestCurrencyStatement {

	AFactory factory;
	BankState bankState;
	
	@Before
	public void setUp() throws Exception {		
		BankState bankState = new BankState();
		factory = new StatementFactory(bankState);
	}

	@Test
	public void testCurrencyStatement() {
		assertTrue("Currency and pos number", factory.satisfies("HUG 100").eval().get("info").equals("HUG 100"));
		assertTrue("Currency and neg number", factory.satisfies("HUD -100").eval().get("info").equals("HUD -100"));
		assertTrue("type mismatch", factory.satisfies("HUG -|100").eval().containsKey("error"));
		assertTrue("type mismatch", factory.satisfies("HuG - 1001").eval().containsKey("error"));
		
	}

}
