package me.tracker.statements;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import me.tracker.factory.AFactory;
import me.tracker.factory.StatementFactory;
import me.tracker.model.BankState;
import me.tracker.statement.QuitStatement;

public class TestQuitStatement {
	
	AFactory factory;
	BankState bankState;

	@Before
	public void setUp() throws Exception {
		bankState = new BankState();
		factory = new StatementFactory(bankState);
	}

	@Test
	public void testQuitStatement() {
		assertTrue("is right type", factory.satisfies("quit") instanceof QuitStatement);
		// eval quit -> exit execution
//		assertTrue("is right result ", factory.satisfies("quit").eval().get("info").equals("bye!"));
		
	}

}
