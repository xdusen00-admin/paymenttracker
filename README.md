# PaymentTracker #

Program that keeps a record of payments. Each payment includes a currency and an amount. The program should output a list of all the currency and amounts to the console once per minute. The input can be typed into the command line, and optionally also be loaded from a file when starting up. Each currency to have the exchange rate compared to USD configured

### TODO ###

* write tests and make skeleton of the application
* keep eyes on easy adding new commands
* describe all possible commands
* write notes how to run the application

### APPLICATION COMMANDS ###

possible commands |  command   | Description                    
:----------------- | :---------  | :------------------------------ 
add amount        |  `XXX Y`  | XXX - 3 uppercase letters Y int amount   
enable exchage print |`defE rate` | E - (enable or disable)   
define rate   	|`defXXX Z` | XXX - 3 uppercase letters Z double rate  
load file		| `load F`	 | F is filepath described below, F can  also default to load last configuration
quit   	| `quit` | quit the program

#### file format #####
to define amount A as INT for currency XXX accepts line `XXX;A`
to define rate R as DOUBLE for currency XXX accepts line `XXX;R`
to define amount A as INT, rate R as DOUBLE for currency XXX accepts line `XXX;A;R`

notice that amount is integer so if you want to define amount must be in integer format
otherwise if it is in double format(0.30) it it will be rate.
unexpected behaviour can occoured when amount is defined as 100.00 the it will be accepted as rate for given currency

lines which do not satisfies given pattern are ignored  

### COMPILATION and RUNNING the APPLICATION ###
 
> + you have ant properly installed then type.
>>     ant
> + run created jar with (optionally with path parameter to file)
>>     java -jar bin/jar/PaymentTracker.jar <path>

### tests ###
please run tests in your IDE under test source folder.
before running tests keep file bankState.csv on classpath.

### a few notes about application ###
Application runs in loop to while reading operations from stdin and evaluate it to Statement. Each statement is created in StatementFactory from (stdin) typed operation. All possible operations are defined in enum called OP by pattern they match. Statements mutate BankState. BankState is passed to Application object and shared between Statement(s). Access to critical methods are synchronized because fields are accessed by another thread simultaneously in defined period so they need to have a fresh written data in consistent state.
